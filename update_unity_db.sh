#!/bin/bash

set -e

rm -rf image/data.old
mv image/data image/data.old
docker cp idmlocal_unity-idm_1:/opt/unity-server/data image/data