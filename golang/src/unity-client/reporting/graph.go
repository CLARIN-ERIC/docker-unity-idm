package reporting

import (
	"fmt"
	"sort"
)

type Graph struct {
	Creation *Data `json:"creation"`
	Conversion *Data `json:"conversion"`
	Status *Data `json:"status"`
}

type Data struct {
	Values []float64 `json:"values"`
	Labels []string `json:"labels"`
}

func NewGraph(entries []*Entry) *Graph {
	//Compute
	creation := map[string]float64{}
	conversion := map[string]float64{}
	status := map[string]float64{}
	for _, entry := range entries{
		year, _ := entry.Created.ISOWeek()
		month := int(entry.Created.Month())

		creation_key := ""
		if month < 10 {
			creation_key = fmt.Sprintf("%d-0%d", year, month)
		} else {
			creation_key = fmt.Sprintf("%d-%d", year, month)
		}
		conversion_key := fmt.Sprintf("%t", entry.Used)
		status_key := entry.State

		if _, ok := creation[creation_key]; !ok {
			creation[creation_key] = 0
		}
		if _, ok := conversion[conversion_key]; !ok {
			conversion[conversion_key] = 0
		}
		if _, ok := status[status_key]; !ok {
			status[status_key] = 0
		}

		creation[creation_key]++
		conversion[conversion_key]++
		status[status_key]++
	}

	return &Graph{convertToData(creation),
		convertToData(conversion),
		convertToData(status),
	}
}

func convertToData(data map[string]float64) *Data {
	values := []float64{}
	keys := []string{}
	for key, _ := range data {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		values = append(values, data[key])
	}

	return &Data{values, keys}
}



