package reporting

import (
	"unity-client/client"
	"time"
	"sort"
	"os"
	"encoding/csv"
	"strconv"
	"fmt"
)

type Reporting struct {

}

func New() *Reporting {
	return &Reporting{}
}

type Entry struct {
	Id int64 `json:"id"`
	Value *string `json:"email"`
	Created time.Time `json:"created"`
	Used bool `json:"used"`
	State string `json:"state"`
}

func (e *Entry) headerSlice() []string {
	result := []string{}
	result = append(result,"Id")
	if e.Value != nil {
		result = append(result, "Email")
	}
	result = append(result,"Created")
	result = append(result,"Created Week")
	result = append(result,"Created Month")
	result = append(result,"Created year")
	result = append(result,"Used")
	result = append(result,"State")
	return result
}

func (e *Entry) asSlice() []string {
	year, week := e.Created.ISOWeek()
	month := int(e.Created.Month())

	result := []string{}
	result = append(result, strconv.FormatInt(e.Id, 10))
	if e.Value != nil {
		result = append(result, *e.Value)
	}
	result = append(result,e.Created.String())
	result = append(result,fmt.Sprintf("%d-%d", year, week))
	result = append(result,fmt.Sprintf("%d-%d", year, month))
	result = append(result, strconv.Itoa(year))
	result = append(result,strconv.FormatBool(e.Used))
	result = append(result,e.State)

	return result
}

func (r *Reporting) GenerateAccountsReport(members []client.GroupMembers, anonimized bool) ([]*Entry) {

	entries := []*Entry{}
	for _, member := range members {
		var entry *Entry = nil
		persistent := false
		for _, identity := range member.Entity.Identities {
			if identity.TypeId == "email" {
				var id *string = nil
				if !anonimized {
					id = &identity.Value
				}

				entry = &Entry{identity.EntityId,
					id,
					time.Unix(0, identity.CreationTs*1000000),
					false,
					member.Entity.EntityInfo.State,
				}
			} else if identity.TypeId == "persistent" {
				persistent = true
			}
		}

		if entry != nil {
			entry.Used = persistent
			entries = append(entries, entry)
		}
	}

	sort.Slice(entries, func(i, j int) bool {
		return entries[i].Created.Before(entries[j].Created)
	})

	return entries
}

func (r *Reporting) Convert(entries []*Entry) [][]string {
	if len(entries) <= 0 {
		return nil
	}

	data := [][]string{entries[0].headerSlice()}
	for _, entry := range entries{
		data = append(data, entry.asSlice())
	}
	return data
}

/**
Output as delimiter separated value
 */
func (r *Reporting) OutputAsDsv(delimiter rune, data [][]string) (error) {
	writer := csv.NewWriter(os.Stdout)
	writer.Comma = delimiter
	defer writer.Flush()

	for _, value := range data {
		err := writer.Write(value)
		if err != nil {
			return err
		}
	}

	return nil
}