package main

import (
	"encoding/json"
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"unity-client/clarin/cli"
	"unity-client/client"
	"unity-client/config"
	"unity-client/reporting"
)

var rootCmd = &cobra.Command{
	Use:   "unity-client",
	Short: "Unity CLI",
	Long:  `Unity CLI`,
}

const (
	default_verbose = false
	default_host    = "localhost"
	default_port    = 2443
)

func main() {
	var verbose bool
	var host string
	var port int

	cfg, err := config.LoadConfigFromHomeDir(".unity/config.toml")
	if err != nil {
		fmt.Printf("Failed to load config file. Error: %s", err)
		os.Exit(1)
	}

	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", default_verbose, "Run in verbose mode")
	rootCmd.PersistentFlags().StringVar(&host, "host", default_host, "Unity instance hostname")
	rootCmd.PersistentFlags().IntVar(&port, "port", default_port, "Unity instance https port")
	rootCmd.AddCommand(versionCmd)
	rootCmd.AddCommand(InitIdentityCommand(cfg, &verbose, &host, &port))
	rootCmd.AddCommand(InitEntityCommand(cfg, &verbose, &host, &port))
	rootCmd.AddCommand(InitGroupCommand(cfg, &verbose, &host, &port))
	rootCmd.AddCommand(InitReportCommand(cfg, &verbose, &host, &port))
	rootCmd.AddCommand(cli.AddClarinCli(cfg, &verbose, &host, &port))
	rootCmd.Execute()
}

func InitIdentityCommand(cfg *config.Config, verbose *bool, hostname *string, port *int) *cobra.Command {
	var scheme string = "https"
	var sslVerify = false

	var identityType string
	var identityValue string

	var cmd = &cobra.Command{
		Use:   "identity",
		Short: "Identity Commands",
		Long:  `Identity Commands`,
	}

	var resolve = &cobra.Command{
		Use:   "resolve",
		Short: "Resolve identity",
		Long:  `Resolve identity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			identity, err := c.IdentityService.Resolve(identityType, identityValue)
			if err != nil {
				fmt.Printf("Failed to resolve identity with type=%s and value=%s. Error: %s\n", identityType, identityValue, err)
				os.Exit(1)
			}

			json_encoded_identity, err := json.MarshalIndent(identity, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode identity as json. Error: %s\n", err)
			} else {
				fmt.Printf("Identity:\n%s\n", string(json_encoded_identity))
			}
		},
	}
	resolve.Flags().StringVarP(&identityType, "type", "t", "email", "Identity type")
	resolve.Flags().StringVarP(&identityValue, "value", "i", "example@clarin.eu", "Identity value")

	cmd.AddCommand(resolve)

	return cmd
}

func InitEntityCommand(cfg *config.Config, verbose *bool, hostname *string, port *int) *cobra.Command {
	var scheme string = "https"
	var sslVerify = false

	var identityType string
	var identityValue string
	var entityId string

	var cmd = &cobra.Command{
		Use:   "entity",
		Short: "Entity Commands",
		Long:  `Entity Commands`,
	}

	cmd.PersistentFlags().StringVarP(&identityType, "type", "t", "", "Identity type")
	cmd.PersistentFlags().StringVarP(&entityId, "id", "d", "1", "Entity id")

	var getEntity = &cobra.Command{
		Use:   "id",
		Short: "Get identity for a specific entity",
		Long:  `Get identity for a specific entity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			identity, err := c.EntityService.GetEntity(entityId, identityType)
			if err != nil {
				fmt.Printf("Failed to get identity of type=%s from entity with id=%s. Error: %s\n", identityType, entityId, err)
				os.Exit(1)
			}

			json_encoded_identity, err := json.MarshalIndent(identity, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode identity as json. Error: %s\n", err)
			} else {
				fmt.Printf("Identity:\n%s\n", string(json_encoded_identity))
			}
		},
	}

	var getEntityGroups = &cobra.Command{
		Use:   "groups",
		Short: "Get all groups for an entity",
		Long:  `Get all groups for an entity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			identity, err := c.EntityService.GetEntityGroups(entityId, identityType)
			if err != nil {
				fmt.Printf("Failed to get identity of type=%s from entity with id=%s. Error: %s\n", identityType, entityId, err)
				os.Exit(1)
			}

			json_encoded_identity, err := json.MarshalIndent(identity, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode identity as json. Error: %s\n", err)
			} else {
				fmt.Printf("Identity:\n%s\n", string(json_encoded_identity))
			}
		},
	}

	var getEntityAttributes = &cobra.Command{
		Use:   "attrs",
		Short: "Get all attributes for an entity",
		Long:  `Get all attributes for an entity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			group := ""
			effective := ""
			identity, err := c.EntityService.GetEntityAttributes(entityId, group, effective, identityType)
			if err != nil {
				fmt.Printf("Failed to get identity of type=%s from entity with id=%s. Error: %s\n", identityType, entityId, err)
				os.Exit(1)
			}

			json_encoded_identity, err := json.MarshalIndent(identity, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode attriobutes as json. Error: %s\n", err)
			} else {
				fmt.Printf("Identity:\n%s\n", string(json_encoded_identity))
			}
		},
	}

	var createEntity = &cobra.Command{
		Use:   "create",
		Short: "Create a new entity",
		Long:  `Create a new entity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			id, err := c.EntityService.CreateEntity(identityType, identityValue, true)
			if err != nil {
				fmt.Printf("Failed to create entity. Error: %s\n", err)
				os.Exit(1)
			}

			fmt.Printf("%d\n", id)
		},
	}
	createEntity.PersistentFlags().StringVar(&identityValue, "value", "", "Identity value")

	cmd.AddCommand(getEntity, getEntityGroups, getEntityAttributes, createEntity)

	return cmd
}

func InitGroupCommand(cfg *config.Config, verbose *bool, hostname *string, port *int) *cobra.Command {
	var scheme string = "https"
	var sslVerify = false

	var group_path string

	var cmd = &cobra.Command{
		Use:   "group",
		Short: "Group Commands",
		Long:  `Group Commands`,
	}
	cmd.PersistentFlags().StringVarP(&group_path, "path", "p", "/", "Group path")

	var resolve = &cobra.Command{
		Use:   "ls",
		Short: "List group information for a  identity",
		Long:  `List group information for a  identity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			identity, err := c.GroupService.List(group_path)
			if err != nil {
				fmt.Printf("Failed to resolve group path=%s. Error: %s\n", group_path, err)
				os.Exit(1)
			}

			json_encoded_identity, err := json.MarshalIndent(identity, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode identity as json. Error: %s\n", err)
			} else {
				fmt.Printf("Identity:\n%s\n", string(json_encoded_identity))
			}
		},
	}

	var members = &cobra.Command{
		Use:   "members",
		Short: "List group information for a  identity",
		Long:  `List group information for a  identity`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			identity, err := c.GroupService.Members(group_path)
			if err != nil {
				fmt.Printf("Failed to resolve group path=%s. Error: %s\n", group_path, err)
				os.Exit(1)
			}

			json_encoded_identity, err := json.MarshalIndent(identity, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode identity as json. Error: %s\n", err)
			} else {
				fmt.Printf("Identity:\n%s\n", string(json_encoded_identity))
			}
		},
	}

	cmd.AddCommand(resolve, members)

	return cmd
}

func InitReportCommand(cfg *config.Config, verbose *bool, hostname *string, port *int) *cobra.Command {
	var scheme string = "https"
	var sslVerify = false
	var group_path string

	var cmd = &cobra.Command{
		Use:   "report",
		Short: "Reporting commands",
		Long:  `Reporting ommands`,
	}
	cmd.PersistentFlags().StringVarP(&group_path, "path", "p", "/", "Group path")

	var accounts = &cobra.Command{
		Use:   "accounts",
		Short: "Report on account creation",
		Long:  `Report on account creation`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			members, err := c.GroupService.Members(group_path)
			if err != nil {
				fmt.Printf("Failed to resolve group path=%s. Error: %s\n", group_path, err)
				os.Exit(1)
			}

			anonimized := true
			output := "json"

			r := reporting.New()
			report := r.GenerateAccountsReport(members, anonimized)

			if output == "json" {
				json_encoded_identity, err := json.MarshalIndent(report, "", "  ")
				if err != nil {
					fmt.Printf("Failed to encode report as json. Error: %s\n", err)
				} else {
					fmt.Printf("%s\n", string(json_encoded_identity))
				}
			} else if output == "csv" || output == "tsv" {
				//Select proper delimiter
				delimiter := ','
				switch output {
				case "csv":
					delimiter = ','
				case "tsv":
					delimiter = '\t'
				}
				//Generate delimiter separated output
				converted := r.Convert(report)
				if err := r.OutputAsDsv(delimiter, converted); err != nil {
					fmt.Printf("Failed to write as csv. Error: %s\n", err)
				}
			}

		},
	}

	var graphs = &cobra.Command{
		Use:   "graphs",
		Short: "Generate account graph data",
		Long:  `Generate account graph data`,
		Run: func(cmd *cobra.Command, args []string) {
			c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
			if err != nil {
				fmt.Printf("Failed to initialize client. Error: %s\n", err)
				os.Exit(1)
			}

			members, err := c.GroupService.Members(group_path)
			if err != nil {
				fmt.Printf("Failed to resolve group path=%s. Error: %s\n", group_path, err)
				os.Exit(1)
			}

			anonimized := true

			r := reporting.New()
			report := r.GenerateAccountsReport(members, anonimized)

			g := reporting.NewGraph(report)

			fmt.Printf("Graph data:\n")
			json_encoded_identity, err := json.MarshalIndent(g, "", "  ")
			if err != nil {
				fmt.Printf("Failed to encode report as json. Error: %s\n", err)
			} else {
				fmt.Printf("%s\n", string(json_encoded_identity))
			}
		},
	}

	cmd.AddCommand(graphs)
	cmd.AddCommand(accounts)

	return cmd
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of unity-client",
	Long:  `All software has versions. This is unity-client's.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		printVersion()
		return nil
	},
}

func printVersion() {
	fmt.Printf("Unity command line interface v%s by CLARIN ERIC\n", "1.0.0-beta")
}
