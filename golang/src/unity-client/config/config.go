package config

import (
	"fmt"
	"github.com/BurntSushi/toml"
	"log"
	"os"
	"path/filepath"
)

type Config struct {
	Username string `toml:"username"`
	Password string `toml:"password"`
}

func LoadConfigFromHomeDir(file string) (*Config, error) {
	return LoadConfig(pathInHomeDir(file))
}

func LoadConfig(file string) (*Config, error) {
	var conf Config
	if _, err := toml.DecodeFile(file, &conf); err != nil {
		return nil, fmt.Errorf("Failed to parse config from %s. Error: %s\n", file, err)
	}
	return &conf, nil
}

func pathInHomeDir(path string) string {
	dirname, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	return filepath.Join(dirname, path)
}
