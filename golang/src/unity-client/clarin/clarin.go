package clarin

import (
	"fmt"
	"math/rand"
	"os"
	"strings"
	"time"
	"unity-client/client"
	"unity-client/config"
)

type Clarin struct {
	client *client.Client
}

func New(cfg *config.Config, verbose *bool, hostname *string, port *int) *Clarin {
	scheme := "https"
	sslVerify := false

	c, err := client.New(scheme, *hostname, *port, cfg.Username, cfg.Password, sslVerify)
	if err != nil {
		fmt.Printf("Failed to initialize client. Error: %s\n", err)
		os.Exit(1)
	}

	return &Clarin{client: c}
}

func (c *Clarin) generateRandomPassword(length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"abcdefghijklmnopqrstuvwxyz" +
		"0123456789")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

/**
 * Generate a number of test users following some specified pattern
 */
func (c *Clarin) GenerateTestUsers(amount int, username_prefix string) ([]int64, error) {
	//Map usernames to entityIds
	nameToEntityId := map[string]int64{}
	nameToPassword := map[string]string{}
	//Generate passwords, create the entity and store in the map
	for i := 0; i < amount; i++ {
		username := fmt.Sprintf("%s%d@clarin.eu", username_prefix, i+1)
		password := c.generateRandomPassword(12)

		entityId, err := c.CreateUser(username, password, true)
		if err != nil {
			fmt.Printf("Failed to create user: %s. Error: %s\n", username, err)
			nameToEntityId[username] = -1
			nameToPassword[username] = password
		} else {
			fmt.Printf("Created user: %s. EntityId=%d, password=%s\n", username, entityId, password)
			nameToEntityId[username] = entityId
			nameToPassword[username] = password
		}
	}

	//Generate result
	ids := []int64{}
	for _, v := range nameToEntityId {
		ids = append(ids, v)
	}
	return ids, nil
}

/**
 * Create a new user with a confirmed email address, a valid password credential and some default
 * attributes.
 */
func (c *Clarin) CreateUser(email_address, password string, confirmed_email bool) (int64, error) {
	//Create new entity with identity of type email
	identityType := "email"
	entityId, err := c.client.EntityService.CreateEntity(identityType, email_address, confirmed_email)
	if err != nil {
		return -1, err
	}

	//Update entity credential
	if err := c.client.EntityService.SetCredential(entityId, "sys%3apassword", password); err != nil {
		return entityId, err
	}

	//Update entity with required attributes
	attrs := []client.Attribute{
		{Name: "name", GroupPath: "/", Values: []string{email_address}},
	}
	if err := c.client.EntityService.SetAttribute(entityId, identityType, attrs); err != nil {
		return entityId, fmt.Errorf("Failed to updated attributes for created entity. error: %s", err)
	}

	return entityId, nil
}

func (c *Clarin) Report() (error) {
    return nil
}
