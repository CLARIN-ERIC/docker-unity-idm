package cli

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"unity-client/clarin"
	"unity-client/config"
)

func AddClarinCli(cfg *config.Config, verbose *bool, hostname *string, port *int) *cobra.Command {
	var username string
	var password string
	var confirm_email bool
	var user_amount int
	var username_prefix string

	var cmdUserCreateRandom = &cobra.Command{
		Use:   "random",
		Short: "Create a number of random test users",
		Long:  `Create a number of random test users`,
		Run: func(cmd *cobra.Command, args []string) {
			_, err := clarin.New(cfg, verbose, hostname, port).GenerateTestUsers(user_amount, username_prefix)
			if err != nil {
				fmt.Printf("Failed to generate user accounts: %s\n", err)
				os.Exit(1)
			}
		},
	}
	cmdUserCreateRandom.Flags().StringVar(&username_prefix, "prefix", "test", "<prefix>xx@clarin.eu, where xx is the sequence number")
	cmdUserCreateRandom.Flags().IntVar(&user_amount, "amount", 5, "Number of test accounts to create")
	
	var cmdUserCreate = &cobra.Command{
		Use:   "create",
		Short: "Create a new user",
		Long:  `Create a new user`,
		Run: func(cmd *cobra.Command, args []string) {
			entityId, err := clarin.New(cfg, verbose, hostname, port).CreateUser(username, password, confirm_email)
			if err != nil {
				fmt.Printf("Failed to create user: %s\n", err)
				os.Exit(1)
			}
			fmt.Printf("Created user. Entity id=%d\n", entityId)
		},
	}
	cmdUserCreate.Flags().StringVar(&username, "username", "", "User email address")
	cmdUserCreate.Flags().StringVar(&password, "password", "", "Password")
	cmdUserCreate.Flags().BoolVar(&confirm_email, "confirmed", true, "Set email address to confirmed")

	var cmdUser = &cobra.Command{
		Use:   "user",
		Short: "User commands",
		Long:  `User commands`,
	}
	cmdUser.AddCommand(cmdUserCreate, cmdUserCreateRandom)

	var cmd = &cobra.Command{
		Use:   "clarin",
		Short: "Clarin Commands",
		Long:  `Clarin Commands`,
	}
	cmd.AddCommand(cmdUser)

	return cmd
}


