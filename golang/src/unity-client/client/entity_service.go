package client

import (
	"encoding/json"
	"fmt"
)

type EntityService struct {
	uhc *UnityHttpClient
}

func (s *EntityService) GetEntity(entityId, identityType string) (*IdentityValue, error) {
	path := fmt.Sprintf("entity/%s", entityId)

	var response_body []byte = nil
	var err error = nil

	if identityType == "" {
		response_body, err = s.uhc.Get(path)
	} else {
		response_body, err = s.uhc.GetWithQuery(path, []QueryItem{{Key: "identityType", Value: identityType}})
	}

	if err != nil {
		return nil, err
	}

	var result IdentityValue
	if err := json.Unmarshal(response_body, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (s *EntityService) GetEntityGroups(entityId, identityType string) ([]string, error) {
	path := fmt.Sprintf("entity/%s/groups", entityId)

	var response_body []byte = nil
	var err error = nil

	if identityType == "" {
		response_body, err = s.uhc.Get(path)
	} else {
		response_body, err = s.uhc.GetWithQuery(path, []QueryItem{{Key: "identityType", Value: identityType}})
	}

	if err != nil {
		return nil, err
	}

	var result []string
	if err := json.Unmarshal(response_body, &result); err != nil {
		return nil, err
	}

	return result, nil
}

func (s *EntityService) GetEntityAttributes(entityId, group, effective, identityType string) ([]*Attribute, error) {
	path := fmt.Sprintf("entity/%s/attributes", entityId)

	var response_body []byte = nil
	var err error = nil

	//if identityType == "" {
	response_body, err = s.uhc.Get(path)
	//} else {
	//	response_body, err = s.uhc.GetWithQuery(path, []QueryItem{{Key: "identityType", Value: identityType}})
	//}

	if err != nil {
		return nil, err
	}

	var result []*Attribute
	if err := json.Unmarshal(response_body, &result); err != nil {
		return nil, err
	}

	return result, nil
}

func (s *EntityService) CreateEntity(identityType, identityValue string, confirmed_email bool) (int64, error) {
	path := fmt.Sprintf("entity/identity/%s/%s", identityType, identityValue)
	if identityType == "email" {
		//Special encoding of emails available in 1.9.x documentation:
		//	https://www.unity-idm.eu/documentation/unity-1.9.6/manual.html#email-encoding
		if confirmed_email {
			path = fmt.Sprintf("%s%s", path, "%5bCONFIRMED%5d")
		} else {
			path = fmt.Sprintf("%s%s", path, "%5bUNCONFIRMED%5d")
		}
	}
	var response_body []byte = nil
	var err error = nil

	response_body, err = s.uhc.PostWithQuery(path, []QueryItem{{Key: "credentialRequirement", Value: "Password requirement"}})
	if err != nil {
		return -1, err
	}

	result := map[string]int64{}
	if err := json.Unmarshal(response_body, &result); err != nil {
		return -1, err
	}

	return result["entityId"], nil
}

func (s *EntityService) SetAttribute(entityId int64, identityType string, attrs []Attribute) error {
	path := fmt.Sprintf("entity/%d/attributes", entityId)

	//_, err := s.uhc.PutWithQuery(path, []QueryItem{{Key: "identityType", Value: "email"}}, attrs)
	_, err := s.uhc.PutWithQuery(path, nil, attrs)
	if err != nil {
		return err
	}

	return nil
}

func (s *EntityService) SetCredential(entityId int64, credentialName, credentialValue string) error {
	path := fmt.Sprintf("entity/%d/credential-adm/%s", entityId, credentialName)
	cred := map[string]string{"password": credentialValue}
	_, err := s.uhc.PutWithQuery(path, nil, cred)
	if err != nil {
		return err
	}
	return nil

}
