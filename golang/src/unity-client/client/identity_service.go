package client

import (
	"fmt"
	"encoding/json"
)

type IdentityService struct {
	uhc *UnityHttpClient
}

func (s *IdentityService) Resolve(identityType, identityValue string) (*IdentityValue, error) {
	path := fmt.Sprintf("resolve/%s/%s", identityType, identityValue)

	response_body, err := s.uhc.Get(path)
	if err != nil {
		return nil, err
	}

	var result IdentityValue
	if err := json.Unmarshal(response_body, &result); err != nil {
		return nil, err
	}

	return &result, nil
}
