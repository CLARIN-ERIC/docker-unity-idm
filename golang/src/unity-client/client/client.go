package client

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Client struct {
	EntityService   *EntityService
	IdentityService *IdentityService
	GroupService    *GroupService
}

func New(scheme, domain string, port int, username, password string, sslVerify bool) (*Client, error) {
	api_base_url, err := url.Parse(fmt.Sprintf("%s://%s:%d/rest-admin/v1/", scheme, domain, port))
	if err != nil {
		return nil, fmt.Errorf("Failed to parse base url. Error: %s", err)
	}

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: !sslVerify},
	}

	unityHttpClient := &UnityHttpClient{
		httpClient: &http.Client{Transport: tr}, //TODO: configure client properties, e.g. timeout?
		api_base:   api_base_url,
		username:   &username,
		password:   &password,
	}

	return &Client{
		IdentityService: &IdentityService{unityHttpClient},
		EntityService:   &EntityService{unityHttpClient},
		GroupService:    &GroupService{unityHttpClient},
	}, nil
}

type QueryItem struct {
	Key   string
	Value string
}

type UnityHttpClient struct {
	httpClient *http.Client
	api_base   *url.URL
	username   *string
	password   *string
}

func (u *UnityHttpClient) request(url *url.URL, method string, req_body []byte) ([]byte, error) {
	var result []byte

	//	fmt.Printf("Request: %s %s, username=%s, password=%s\n", method, url.String(), *u.username, *u.password)

	req, err := http.NewRequest(method, url.String(), bytes.NewReader(req_body))
	if err != nil {
		return result, err
	}
	req.Header.Add("Accept", "application/json")
	if req_body != nil {
		req.Header.Add("Content-Type", "application/json")
	}
	req.SetBasicAuth(*u.username, *u.password)

	//Execute request
	resp, err := u.httpClient.Do(req)
	if err != nil {
		return result, err
	}
	defer resp.Body.Close()

	//Check for invalid status codes
	if resp.StatusCode >= 400 && resp.StatusCode <= 500 {
		return result, fmt.Errorf("Invalid response: %s", resp.Status)
	} else if resp.StatusCode >= 500 && resp.StatusCode <= 600 {
		return result, fmt.Errorf("Invalid response: %s", resp.Status)
	}

	//Process response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return result, err
	}

	return body, nil
}

func (u *UnityHttpClient) Get(path string) ([]byte, error) {
	return u.GetWithQuery(path, nil)
}

func (u *UnityHttpClient) GetWithQuery(path string, query []QueryItem) ([]byte, error) {
	return u.Request("GET", path, query, nil)
}

func (u *UnityHttpClient) Post(path string) ([]byte, error) {
	return u.PostWithQuery(path, nil)
}

func (u *UnityHttpClient) PostWithQuery(path string, query []QueryItem) ([]byte, error) {
	return u.Request("POST", path, query, nil)
}

func (u *UnityHttpClient) PutWithQuery(path string, query []QueryItem, body interface{}) ([]byte, error) {
	return u.Request("PUT", path, query, body)
}

func (u *UnityHttpClient) Request(method, path string, query []QueryItem, body interface{}) ([]byte, error) {
	//Prepare request
	path_url, err := url.Parse(path)
	if err != nil {
		return nil, fmt.Errorf("Failed to parse path url. Error: %s", err)
	}

	request_url := u.api_base.ResolveReference(path_url)

	//Add query parameters
	params := url.Values{}
	for _, item := range query {
		params.Add(item.Key, item.Value)
	}
	request_url.RawQuery = params.Encode()

	var request_body []byte = nil

	if body != nil {
		json_bytes, err := json.Marshal(body)
		if err == nil {
			request_body = json_bytes
		}
	}
	//Issue request
	response_body, err := u.request(request_url, method, request_body)
	if err != nil {
		return nil, fmt.Errorf("%s request to %s failed. Error: %s", method, request_url.String(), err)
	}

	return response_body, nil
}

type IdentityValue struct {
	EntityInfo *EntityInformation `json:"entityInformation"`
	Identities []*Identity        `json:"identities"`
}

type EntityInformation struct {
	State string `json:"state"`
	Id    int64  `json:"entityId"`
}

type Identity struct {
	Value            string            `json:"value"`
	ConfirmationInfo *ConfirmationInfo `json:"confirmationInfo"`
	ComparableValue  string            `json:"comparableValue"`
	CreationTs       int64             `json:"creationTs"`
	UpdateTs         int64             `json:"updateTs"`
	TypeId           string            `json:"typeId"`
	EntityId         int64             `json:"entityId"`
}

type ConfirmationInfo struct {
	Confirmed         bool  `json:"confirmed"`
	ConfirmationDate  int64 `json:"confirmationDate"`
	SentRequestAmount int64 `json:"sentRequestAmount"`
}

type CredentialInfo struct {
	CredentialRequirementId string                     `json:"credentialRequirementId"`
	CredentialState         map[string]CredentialState `json:"credentialsState"`
}

type CredentialState struct {
	State            string `json:"state"`
	ExtraInformation string `json:"extraInformation"`
}

type Attribute struct {
	Values      []string `json:"values"`
	CreationTs  *int64   `json:"creationTs,omitempty"`
	UpdateTs    *int64   `json:"updateTs,omitempty"`
	Direct      *bool    `json:"direct,omitempty"`
	Name        string   `json:"name"`
	GroupPath   string   `json:"groupPath"`
	ValueSyntax *string  `json:"valueSyntax,omitempty"`
}

type Group struct {
	SubGroups []string       `json:"subGroups"`
	Members   []*GroupMember `json:"members"`
}

type GroupMember struct {
	CreationTs int64  `json:""`
	Group      string `json:"group"`
	EntityId   int64  `json:"entityId"`
}

type GroupMembers struct {
	Group string `json:"group"`
	//Entity *EntityInformation `json:"entity"`
	Entity     *IdentityValue `json:"entity"`
	Attributes []*Attribute   `json:"attributes"`
}
