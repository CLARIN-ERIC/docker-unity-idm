package client

import (
	"encoding/json"
	"fmt"
	"net/url"
)

type GroupService struct {
	uhc *UnityHttpClient
}

func (s *GroupService) List(group_path string) (*Group, error) {
	path := fmt.Sprintf("group/%s", url.QueryEscape(group_path))

	response_body, err := s.uhc.Get(path)

	if err != nil {
		return nil, err
	}

	var result Group
	if err := json.Unmarshal(response_body, &result); err != nil {
		return nil, err
	}

	return &result, nil
}

func (s *GroupService) Members(group_path string) ([]GroupMembers, error) {
	path := fmt.Sprintf("group-members/%s", url.QueryEscape(group_path))

	response_body, err := s.uhc.Get(path)

	if err != nil {
		return nil, err
	}

	var result []GroupMembers
	if err := json.Unmarshal(response_body, &result); err != nil {
		return nil, err
	}

	return result, nil
}
