#!/bin/sh

DATE=$(date '+%Y%m%d_%H%M%S')
cd "${UNITY_DIR}/data" && \
tar -pczvf "/backups/unity-idm_${DATE}.tar.gz" .