#!/bin/bash

set -e

_JVM_MAX_HEAP=${JVM_MAX_HEAP:-"2g"}

_FLAG_DEBUG=${FLAG_DEBUG:-"0"}
_FLAG_DEBUG_SUSPEND=${FLAG_DEBUG_SUSPEND:-"0"}
_FLAG_SKIP_METADATA=${FLAG_SKIP_METADATA:-"0"}
_FLAG_SKIP_WAIT_DATABASE=${FLAG_SKIP_WAIT_DATABASE:-"0"}
_FLAG_INIT_CONFIG=${FLAG_INIT_CONFIG:-1}
_FLAG_COMPILE_SASS=${FLAG_COMPILE_SASS:-0}

SP_METADATA_URL="https://shibboleth-sp/Shibboleth.sso/Metadata"
SP_METADATA_FILE_DEST="/opt/unity-server/metadata/sp-metadata.xml"

HOST=${_HOST:-"test"}

ADMIN_PASSWORD=${_ADMIN_PASSWORD:-"Admin12345"}
ENTITY_ID=${_ENTITY_ID:-"http://example-saml-idp.org"}
MAIN_PKI_KEY_ALIAS=${_PKI_KEY_ALIAS:-"unity-test-server"}
#MAIN_PKI_PASSWORD=${_PKI_PASSWORD:-"the!unity"}
UNITY_APPLY_FILE_CONFIG=${_UNITY_APPLY_FILE_CONFIG:-"true"}

MAIN_PKI_KEY_ALIAS=${_MAIN_PKI_KEY_ALIAS:-"unity-test-server-ssl"}
MAIN_PKI_KEY_PASSWORD=${_MAIN_PKI_KEY_PASSWORD:-"the!unity_ssl"}
IDP_PKI_KEY_ALIAS=${_IDP_PKI_KEY_ALIAS:-"unity-test-server"}
IDP_PKI_PASSWORD=${_IDP_PKI_PASSWORD:-"the!unity"}

#LDAP_HOSTNAME=${_LDAP_HOSTNAME:-"0.0.0.0"}
#LDAP_PORT=${_LDAP_PORT:-"10000"}
#LDAP_SECURITY=${_LDAP_SECURITY:-"SSL"} #Allowed values: NONE, SSL, TLS

DEFAULT_SP_METADATA_URLS=
SP_METADATA_URLS=${_SP_METADATA_URLS:-$DEFAULT_SP_METADATA_URLS}

MAIL_SMTP_SERVER=${_MAIL_SMTP_SERVER:-"smtp.transip.email"}
MAIL_SMTP_PORT=${_MAIL_SMTP_PORT:-"465"}
MAIL_SMTP_PASSWORD=${_MAIL_SMTP_PASSWORD:-"this_needs_to_be_set"}
MAIL_SMTP_USERNAME=${_MAIL_SMTP_USERNAME:-"website@clarin.eu"}
MAIL_FROM=${_MAIL_FROM:-"accounts@clarin.eu"}

BASE_DIR="/opt/unity-server"
BASE_CONF_DIR="${BASE_DIR}/conf"
SERVER_CONF_FILE="${BASE_DIR}/conf/unityServer.conf"
MAIL_CONF_FILE="${BASE_CONF_DIR}/mail.properties"
IDP_CONF_FILE="${BASE_CONF_DIR}/modules/saml/saml-webidp.properties"
#LDAP_CONF_FILE="${BASE_CONF_DIR}/modules/ldap-server/ldap.properties"
#INIT_FILE="/data/unity-server/conf/initialised"
#PKI_FILE="/opt/unity-server/conf/pki.properties"
PKI_FILE="pki.properties"

UNITY_LOG_FILE="${BASE_CONF_DIR}/log4j2.xml"
_LOG_LEVEL_ROOT=${LOG_LEVEL_ROOT:-INFO}
_LOG_LEVEL_UNITY_SERVER=${LOG_LEVEL_UNITY_SERVER:-INFO}
_LOG_LEVEL_UNITY_SERVER_LDAP=${LOG_LEVEL_UNITY_SERVER_LDAP:-INFO}
_LOG_LEVEL_UNITY_SERVER_SAML=${LOG_LEVEL_UNITY_SERVER_SAML:-INFO}

_PIWIK_SITE_ID=${PIWIK_SITE_ID:-1}

_UNITY_CORE_ALLOWFULLHTML=${UNITY_CORE_ALLOWFULLHTML:-true}
_UNITY_CORE_DEFAULTWEBPATH=${UNITY_CORE_DEFAULTWEBPATH:-"/home"}

#
# Replace properties old value with new value
#
# Parameters:
#   property name
#   old value
#   new value
#   configuration file
#
function replace {
    echo "[INIT] replace ${1} in ${4}"

    #Escape forward slashes in any replacement value
    OLD_VALUE=$(echo "$2" | sed 's/\//\\\//g')
    NEW_VALUE=$(echo "$3" | sed 's/\//\\\//g')

    sed -i "s/${1}=${OLD_VALUE}/${1}=${NEW_VALUE}/g" "${4}"
}

function replace_log_level {
    PATTERN="${1}"
    REPLACE="${2}"
    FILE="${3}"

    SED_EXPR="s/(${PATTERN}\\\").+(\\\")/\1${REPLACE}\2/"
    sed -r -i "${SED_EXPR}" "${FILE}"
}

function download_sp_metadata {
    printf "[INIT] Downloading SAML SP metadata\n"
    if [ ! -f ${SP_METADATA_FILE_DEST} ]; then
        printf "[INIT] Waiting for SP."
        until curl --output /dev/null --silent --head --fail --insecure "${URL}"; do
            printf '.'
            sleep 1
        done
        printf " Done\n"

        printf "[INIT] Downloading metadata from %s." "${SP_METADATA_URL}"
        curl -o ${SP_METADATA_FILE_DEST}.tmp --insecure --silent ${SP_METADATA_URL}
        #echo '<?xml version="1.0" encoding="UTF-8"?>' >> ${SP_METADATA_FILE_DEST}
        #echo '<md:EntitiesDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:mdattr="urn:oasis:names:tc:SAML:metadata:attribute" xmlns:mdrpi="urn:oasis:names:tc:SAML:metadata:rpi" xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:shibmd="urn:mace:shibboleth:metadata:1.0" xmlns:xrd="http://docs.oasis-open.org/ns/xri/xrd-1.0">' >> ${SP_METADATA_FILE_DEST}
        {
          echo '<?xml version="1.0" encoding="UTF-8"?>';
          echo '<md:EntitiesDescriptor xmlns:md="urn:oasis:names:tc:SAML:2.0:metadata" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:mdattr="urn:oasis:names:tc:SAML:metadata:attribute" xmlns:mdrpi="urn:oasis:names:tc:SAML:metadata:rpi" xmlns:mdui="urn:oasis:names:tc:SAML:metadata:ui" xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion" xmlns:shibmd="urn:mace:shibboleth:metadata:1.0" xmlns:xrd="http://docs.oasis-open.org/ns/xri/xrd-1.0">';
          cat ${SP_METADATA_FILE_DEST}.tmp;
          echo '</md:EntitiesDescriptor>';
        } >> ${SP_METADATA_FILE_DEST}
        #cat ${SP_METADATA_FILE_DEST}.tmp >> ${SP_METADATA_FILE_DEST}
        #echo '</md:EntitiesDescriptor>' >> ${SP_METADATA_FILE_DEST}
        sed -i.bak "s/shibboleth-sp/${_SP_SERVERNAME}/g" ${SP_METADATA_FILE_DEST}
        printf ' Done\n'
    else
        printf "[INIT] Metadata file [%s] already exists\n" "${SP_METADATA_FILE_DEST}"
    fi
}

function restore {
    echo "Listing /backups" && ls -l /backups
    if [ -e "/backups/restore" ]; then
        echo "Restoring backup"
        if [ -e "${UNITY_DIR}/data" ]; then
            echo "  Clearing existing data directory (${UNITY_DIR}/data)"
            rm -f "${UNITY_DIR}/data/*"
        fi

        echo "  Extracting"
        tar -x -C "${UNITY_DIR}/data" -f /backups/restore
        rm /backups/restore
    else
        echo "No backup found to restore"
    fi
}

#
# Update configuration and compile SASS theme if the INIT_FILE doesn't exist
#
function update_configuration {
    #jvm parameters
    update_conf_key_value "MEM" "-Xmx${_JVM_MAX_HEAP}" "${SERVER_CONF_FILE}"

    #logging configuration
    replace_log_level "Root level=" "${_LOG_LEVEL_ROOT}" "${UNITY_LOG_FILE}"
    replace_log_level "name=\"unity.server\" level=" "${_LOG_LEVEL_UNITY_SERVER}" "${UNITY_LOG_FILE}"
    replace_log_level "name=\"unity.server.saml\" level=" "${_LOG_LEVEL_UNITY_SERVER_SAML}" "${UNITY_LOG_FILE}"
    replace_log_level "name=\"unity.server.ldap\" level=" "${_LOG_LEVEL_UNITY_SERVER_LDAP}" "${UNITY_LOG_FILE}"

    #Main server config
    update_conf_key_value "unityServer.core.httpServer.host" "0.0.0.0" "${SERVER_CONF_FILE}"
    update_conf_key_value "unityServer.core.httpServer.port" "2443" "${SERVER_CONF_FILE}"
    update_conf_key_value "unityServer.core.httpServer.advertisedHost" "${HOST}" "${SERVER_CONF_FILE}"
    update_conf_key_value "unityServer.core.initialAdminPassword" "${ADMIN_PASSWORD}" "${SERVER_CONF_FILE}"
    update_conf_key_value "unityServer.core.initialAdminOutdated" "false" "${SERVER_CONF_FILE}"
    #Force config file updates to be applied to the database (will overwrite any changes only made into the database)
    #See: https://www.unity-idm.eu/documentation/unity-3.4.0/manual.html#ver-update
    update_conf_key_value "unityServer.core.useConfiguredContentsOnFreshStartOnly" "${UNITY_APPLY_FILE_CONFIG}" "${SERVER_CONF_FILE}"
    update_conf_key_value "unityServer.core.allowFullHtml" "${_UNITY_CORE_ALLOWFULLHTML}" "${SERVER_CONF_FILE}"
    update_conf_key_value "unityServer.core.defaultWebPath" "${_UNITY_CORE_DEFAULTWEBPATH}" "${SERVER_CONF_FILE}"

    #Database config
    if [ "${_DB_HOST}" != "" ]; then
        update_conf_key_value "unityServer.storage.engine" "rdbms" "${SERVER_CONF_FILE}"
        update_conf_key_value "unityServer.storage.engine.rdbms.jdbcUrl" "jdbc:postgresql://${_DB_HOST}:${_DB_PORT}/${_DB_NAME}" "${SERVER_CONF_FILE}"
        update_conf_key_value "unityServer.storage.engine.rdbms.dialect" "psql" "${SERVER_CONF_FILE}"
        update_conf_key_value "unityServer.storage.engine.rdbms.username" "${_DB_USER}" "${SERVER_CONF_FILE}"
        update_conf_key_value "unityServer.storage.engine.rdbms.password" "${_DB_PASSWORD=}" "${SERVER_CONF_FILE}"
    fi

    #SAML IdP settings
    update_conf_key_value "unity.saml.issuerURI" "${ENTITY_ID}" "${IDP_CONF_FILE}"

#    #Configure ldap authenticator
#    update_conf_key_value 'unityServer.core.authenticators.ldap.authenticatorName' 'pwdLdapSimple' "${SERVER_CONF_FILE}"
#    update_conf_key_value 'unityServer.core.authenticators.ldap.authenticatorType' 'password' "${SERVER_CONF_FILE}"
#    update_conf_key_value 'unityServer.core.authenticators.ldap.localCredential' 'sys:password' "${SERVER_CONF_FILE}"
#    update_conf_key_value 'unityServer.core.authenticators.ldap.retrievalConfigurationFile' '${CONF}/authenticators/passwordRetrieval.properties' "${SERVER_CONF_FILE}"

#    #Load ldap server module
#    update_conf_key_value '$include.ldap-endpoint' '${CONF}/modules/ldap-endpoint.module' "${SERVER_CONF_FILE}"

#    #Setup ldap server module
#    update_conf_key_value 'unityServer.core.endpoints.ldapServer.endpointType' 'LDAPServer' "/opt/unity-server/conf/modules/ldap-endpoint.module"
#    update_conf_key_value 'unityServer.core.endpoints.ldapServer.endpointConfigurationFile' '${CONF}/modules/ldap-server/ldap.properties' "/opt/unity-server/conf/modules/ldap-endpoint.module"
#    update_conf_key_value 'unityServer.core.endpoints.ldapServer.contextPath' '/ldap' "/opt/unity-server/conf/modules/ldap-endpoint.module"
#    update_conf_key_value 'unityServer.core.endpoints.ldapServer.endpointName' 'Ldap Server endpoint' "/opt/unity-server/conf/modules/ldap-endpoint.module"
#    update_conf_key_value 'unityServer.core.endpoints.ldapServer.endpointRealm' 'defaultRealm' "/opt/unity-server/conf/modules/ldap-endpoint.module"
#    update_conf_key_value 'unityServer.core.endpoints.ldapServer.endpointAuthenticators' 'pwdLdapSimple' "/opt/unity-server/conf/modules/ldap-endpoint.module"

#    #Update LDAP configuration
#    update_conf_key_value "unity.ldapServer.host" "${LDAP_HOSTNAME}" "${LDAP_CONF_FILE}"
#    update_conf_key_value "unity.ldapServer.port" "${LDAP_PORT}" "${LDAP_CONF_FILE}"
#    if [ "${LDAP_SECURITY}" == "NONE" ]; then
#        update_conf_key_value "unity.ldapServer.ldaps.enabled" "false" "${LDAP_CONF_FILE}"
#        update_conf_key_value "unity.ldapServer.starttls.enabled" "false" "${LDAP_CONF_FILE}"
#        update_conf_key_value "unity.ldapServer.starttls.force_confidentiality" "false" "${LDAP_CONF_FILE}"
#    elif [ "${LDAP_SECURITY}" == "SSL" ]; then
#        update_conf_key_value "unity.ldapServer.ldaps.enabled" "true" "${LDAP_CONF_FILE}"
#        update_conf_key_value "unity.ldapServer.starttls.enabled" "false" "${LDAP_CONF_FILE}"
#        update_conf_key_value "unity.ldapServer.starttls.force_confidentiality" "false" "${LDAP_CONF_FILE}"
#    elif [ "${LDAP_SECURITY}" == "TLS" ]; then
#        update_conf_key_value "unity.ldapServer.ldaps.enabled" "false" "${LDAP_CONF_FILE}"
#        update_conf_key_value "unity.ldapServer.starttls.enabled" "true" "${LDAP_CONF_FILE}"
#        update_conf_key_value "unity.ldapServer.starttls.force_confidentiality" "true" "${LDAP_CONF_FILE}"
#    fi

    #Update SAML metadata locations
    if ! grep "#unity.saml.acceptedSPMetadataSource.1.url" ${IDP_CONF_FILE}; then
        #Disable placeholder
        sed -i "s/unity.saml.acceptedSPMetadataSource.1.url/#unity.saml.acceptedSPMetadataSource.1.url/g" "${IDP_CONF_FILE}"
        #Ensure we have a newline
        echo "" >> "${IDP_CONF_FILE}"
        #Append all metadata urls
        idx=1
        for i in $SP_METADATA_URLS; do
            echo "Appending $i"
            echo "unity.saml.acceptedSPMetadataSource.${idx}.url=$i" >> "${IDP_CONF_FILE}"
            #let idx=${idx}+1
            (( id++ )) || true
        done
    fi

    #Update mail configuration
    update_conf_key_value "mail.from" "${MAIL_FROM}" "${MAIL_CONF_FILE}"
    update_conf_key_value "mail.smtp.host" "${MAIL_SMTP_SERVER}" "${MAIL_CONF_FILE}"
    update_conf_key_value "mail.smtp.port" "${MAIL_SMTP_PORT}" "${MAIL_CONF_FILE}"
    update_conf_key_value "mailx.smtp.auth.username" "${MAIL_SMTP_USERNAME}" "${MAIL_CONF_FILE}"
    update_conf_key_value "mailx.smtp.auth.password" "${MAIL_SMTP_PASSWORD}" "${MAIL_CONF_FILE}"
    update_conf_key_value "mail.smtp.starttls.enable" "false" "${MAIL_CONF_FILE}"
    update_conf_key_value "mail.smtp.trustAll" "true" "${MAIL_CONF_FILE}"
    update_conf_key_value "mail.smtp.ssl.enable" "true" "${MAIL_CONF_FILE}"
    update_conf_key_value "mail.smtp.auth" "true" "${MAIL_CONF_FILE}"

    #Update pki config
    echo "[INIT] PKI configuration"
    #source /scripts/keystore.bash
    add_credential "MAIN" "pkcs12" "mainKeystore.p12" "${MAIN_PKI_KEY_ALIAS}" "${MAIN_PKI_KEY_PASSWORD}" "${PKI_FILE}"
    add_credential "SAML" "pkcs12" "idpKeystore.p12" "${IDP_PKI_KEY_ALIAS}" "${IDP_PKI_PASSWORD}" "${PKI_FILE}"
    update_conf_key_value "unity.saml.credential" "SAML" "${IDP_CONF_FILE}"

    #Disable other languages
    sed -i 's/unityServer.core.enabledLocales.2/#unityServer.core.enabledLocales.2/g' "${SERVER_CONF_FILE}"
    sed -i 's/unityServer.core.enabledLocales.3/#unityServer.core.enabledLocales.3/g' "${SERVER_CONF_FILE}"

    #Enable piwik with valid site id
    #sed -i "s/_paq.push(['setSiteId', -1]);/_paq.push(['setSiteId', ${_PIWIK_SITE_ID}]);/g" "${UNITY_DIR}/webContents/templates/default.ftl"
    sed -i "s/'setSiteId', -1/'setSiteId', ${_PIWIK_SITE_ID}/g" "${UNITY_DIR}/webContents/templates/default.ftl"

    #https://www.unity-idm.eu/documentation/unity-3.0.0/manual.html#_update

    #Enable clarin custom theme
    #update_conf_key_value 'unityServer.core.defaultWebContentDirectory' 'webContents' "${SERVER_CONF_FILE}"
    #update_conf_key_value 'unityServer.core.defaultTheme' 'clarinTheme' "${SERVER_CONF_FILE}"
    #update_conf_key_value 'unityServer.web.mainTheme' 'clarinTheme' "${SERVER_CONF_FILE}"

    #Override i18n properties
    update_conf_key_value "AuthenticationUI.username" "Email" "${BASE_CONF_DIR}/i18n/webui/messages.properties"
    update_conf_key_value "UsernameIdentityEditor.usernameEmpty" "Email can not be empty" "${BASE_CONF_DIR}/i18n/webui/messages.properties"
    update_conf_key_value "AuthenticationUI.authnenticateButton" "Authenticate" "${BASE_CONF_DIR}/i18n/webui/messages.properties"
    update_conf_key_value "AuthenticationUI.gotoSignUp" "Register a new account" "${BASE_CONF_DIR}/i18n/webui/messages.properties"
    update_conf_key_value "CredentialReset.username" "Your email address" "${BASE_CONF_DIR}/i18n/webui/messages.properties"


    rm -f "${BASE_DIR}/webContents/VAADIN/themes/unityThemeValo/style.css"
    sed -i 's/@import "color-scheme-celadon";/\/\/@import "color-scheme-celadon";/g' "${BASE_DIR}/webContents/VAADIN/themes/common/_configuration.scss"
    if [ ! -f "${BASE_DIR}/webContents/VAADIN/themes/unityThemeValo/style.css" ]; then
        printf "Comiling SASS theme"
        (cd "${BASE_DIR}" && bin/unity-idm-scss-compile webContents/VAADIN/themes/unityThemeValo)
    fi

    #Finish initialization
    cd /opt/unity-server

    echo "[INIT] **************************"
    echo "[INIT] * Done"
    echo "[INIT] **************************"
}

#
# Update the VAADIN template to set the version based on tag written into the image at /VERSION during build
#
function update_version {
    echo "Updating version"
    if [ -f /opt/unity-server/webContents/templates/default.ftl.template ]; then
      cp /opt/unity-server/webContents/templates/default.ftl.template /opt/unity-server/webContents/templates/default.ftl
    fi

    if [ -f "/VERSION" ]; then
        replaceVarInFile "VERSION" "$(cat /VERSION)" /opt/unity-server/webContents/templates/default.ftl
    fi
}

#Compile CLARIN SASS theme
#function sass_compile {
#    cd /opt/unity-server/bin &&
#    /opt/unity-server/bin/unity-idm-scss-compile /opt/unity-server/webContents/VAADIN/themes/clarinTheme
#}

function update_debug_settings {
     #Apply debug settings
    UNITY_STARTUP_PROPERTIES_FILE="/opt/unity-server/conf/startup.properties"
    DEBUG_PREFIX="#"
    if [[ "${_FLAG_DEBUG}" -eq 1 ]]; then
        DEBUG_PREFIX=""
    fi
    JPDA_TRANSPORT="dt_socket"
    JPDA_ADDRESS="*:6009"
    JPDA_SERVER="y"
    JPDA_SUSPEND="n"
    if [[ "${_FLAG_DEBUG_SUSPEND}" -eq 1 ]]; then
        JPDA_SUSPEND="y"
    fi

    #MATCH='^#?OPTS=\$OPTS" -Xrunjdwp:transport=(.+),address=(.+),server=(y|n),suspend=(y|n)"'
    #REPLACE="${DEBUG_PREFIX}OPTS=\$OPTS"'"'" -Xdebug -Xrunjdwp:transport=${JPDA_TRANSPORT},address=${JPDA_ADDRESS},server=${JPDA_SERVER},suspend=${JPDA_SUSPEND}"'"'
    REPLACE="${DEBUG_PREFIX}OPTS=\$OPTS"'"'" -agentlib:jdwp=transport=${JPDA_TRANSPORT},address=${JPDA_ADDRESS},server=${JPDA_SERVER},suspend=${JPDA_SUSPEND}"'"'

    #sed -r -i "s/${MATCH}/${REPLACE}/g" "${UNITY_STARTUP_PROPERTIES_FILE}"
    echo "${REPLACE}" >> "${UNITY_STARTUP_PROPERTIES_FILE}"

    #MATCH='^#?OPTS=\$OPTS" -Xdebug"'
    #REPLACE="${DEBUG_PREFIX}OPTS=\$OPTS"'"'" -Xdebug"'"'
    #sed -r -i "s/${MATCH}/${REPLACE}/g" "${UNITY_STARTUP_PROPERTIES_FILE}"

}

# Copy any files from ${src_dir} into ${dest_dir}, ${dest_dir} will be created if it does not exist.
# No action is performed if ${src_dir} does not exist or does not contain any files.
function move_data_in_place {
    src_dir="${1}"
    dest_dir="${2}"
    msg="${3}"

    printf "[INIT] %s\n" "${msg}"
    mkdir -p "${dest_dir}"
    find "${src_dir}" -maxdepth 1 -type f -print -exec cp {} "${dest_dir}" \;

#    if [ -d "${src_dir}" ]; then
#        printf "[INIT] %s\n" "${msg}"
##        for filename in $(find "${src_dir}" -maxdepth 1 -type f); do
##            dest_file="${dest_dir}/$(basename ${filename})"
##            printf "[INIT] %s -> %s\n" "${filename}" "${dest_file}"
##            cp "${filename}" "${dest_file}"
##        done
#
#        while IFS= read -r -d '' filename
#        do
#          dest_file="${dest_dir}"/$(basename "${filename}")
#          printf "[INIT] %s -> %s\n" "${filename}" "${dest_file}"
#          cp "${filename}" "${dest_file}"
#        done <   <(find "${src_dir}" -maxdepth 1 -type f)
#    fi
}

#Load additional scripts
source /scripts/keystore.bash

#Put any host mounted into the appropriate place
pki_stores_dir="/opt/unity-server/conf/pki/stores"
move_data_in_place "/pki-input" "${pki_stores_dir}" "Moving host mounted pki stores into place"

saml_metadata_dir="/opt/unity-server/metadata"
move_data_in_place "/metadata" "${saml_metadata_dir}" "Moving host mounted SAML metadata into place"

#
# Metadata exchange
#
if [ "${_FLAG_SKIP_METADATA}" == "0" ]; then
    printf "[INIT] Starting metadata exchange\n"
    download_sp_metadata
    printf "[INIT] Finished metadata exchange\n"
else
    printf "[INIT] Skipping metadata exchange\n"
fi

#
# Configuration update
#
if [ "${_FLAG_INIT_CONFIG}" == "1" ]; then
    printf "[INIT] Starting configuration update\n"
    update_configuration
    printf "[INIT] Finished configuration update\n"
else
    printf "[INIT] Skipping configuration update\n"
fi

if [ "${_FLAG_COMPILE_SASS}" == "1" ]; then
    printf "[INIT] Starting sass compilation\n"
    sass_compile
    printf "[INIT] Finished sass compilation\n"
else
    printf "[INIT] Skipping sass compilation\n"
fi

if [ ! -f "/root/.unity/config.toml" ]; then
  mkdir -p "/root/.unity"
  printf 'username="admin"\n' >> "/root/.unity/config.toml"
  printf 'password="%s"' "${_ADMIN_PASSWORD}" >> "/root/.unity/config.toml"
fi

update_version
update_debug_settings
restore

if [ "${_FLAG_SKIP_WAIT_DATABASE}" == "0" ]; then
    #Wait for database to become available
    while ! nc "${_DB_HOST}" "${_DB_PORT}" <<<"exit"; do echo 'Waiting for database to become available'; sleep "1"; done
fi

echo "Database response ok, continue startup..."