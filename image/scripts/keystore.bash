#!/usr/bin/env bash

# Reference: https://superuser.com/questions/724986/how-to-use-password-argument-in-via-command-line-to-openssl-for-decryption
# echo "" > password.txt
# echo "" > password_pkcs12.txt
# Generate a self signed certificate:
#   openssl req -x509 -newkey rsa:4096 -keyout key_clarin.pem -out cert_clarin.pem -sha256 -days 3650 -subj '/CN=unity' -passout file:password.txt
# Generating a pkcs12 keystore:
#   openssl pkcs12 -export -in cert_clarin.pem -inkey key_clarin.pem -out idpKeystore.p12 -name clarin -passin file:password.txt -passout file:password_pkcs12.txt

function setup_self_signed_keystore {
  file=${1}
  alias=${2}
  key_password="${3}"
  store_password="${4}"
  file_postfix="${5}"

  printf "[%s] Generating new credential: %s\n" "$(date)" "${file_postfix}" >> x509.log

  echo "${key_password}" > "password_key_${file_postfix}.txt"
  openssl req -x509 -newkey rsa:4096 -keyout "key_${file_postfix}.pem" -out "cert_${file_postfix}.pem" \
    -sha256 -days 3650 -subj '/CN=unity' -passout "file:password_key_${file_postfix}.txt" >> x509.log 2>&1

  echo "${store_password}" > "password_${file_postfix}_pkcs12.txt"
  openssl pkcs12 -export \
    -in "cert_${file_postfix}.pem" -inkey "key_${file_postfix}.pem" \
    -out "${file}" -name "${alias}" \
    -passin "file:password_key_${file_postfix}.txt" -passout "file:password_${file_postfix}_pkcs12.txt" >> x509.log 2>&1

  #Cleanup all intermediate files
  #Todo: how to ensure this always happens (also in case of errors)?
  rm "password_${file_postfix}_pkcs12.txt"
  rm "password_key_${file_postfix}.txt"
  rm "key_${file_postfix}.pem"
  rm "cert_${file_postfix}.pem"
}

#
# Add or replace any key=* with key=value
# If it was commented out with a '#' it will be enabled
#
function update_conf_key_value {
    key="$1"
    value="$2"
    pki_properties_file="$3"

    #Add or update key=value pair
    #key_exists=$(grep "${key}" "${pki_properties_file}" | wc -l | xargs)
    key_exists=$(grep -c "${key}" "${pki_properties_file}" | xargs)
    if [ "${key_exists}" == "0" ]; then
        echo "${key}=${value}" >> "${pki_properties_file}"
    else
        normalized_key=$(printf '%s' "${key}" | sed -e 's/[]\/$*.^[]/\\&/g');
        normalized_value=$(printf '%s' "${value}" | sed -e 's/[]\/$*.^[]/\\&/g');
        #remove comment if it was commented out
        sed -i "s/^#${normalized_key}/${normalized_key}/g" "${pki_properties_file}"
        #replace value for the specified key
        sed -i "/^${normalized_key}/s/=.*$/=${normalized_value}/" "${pki_properties_file}"
    fi
}

# Add (
function add_credential {
  id="${1}"
  type="${2}"
  file="conf/pki/stores/${3}"
  alias="${4}"
  password="${5}"
  pki_properties_file="conf/${6}"

  printf "[%5s] Checking credential. File=%s\n" "${id}" "${file}"
  if [ ! -f "${file}" ]; then
      printf "[%5s]   Generating new keystore in %s\n" "${id}" "${file}"
      if [ "${password}" == "" ]; then
        printf "[%5s] Generating new password\n" "${id}"
        password=$(openssl rand -base64 32)
      fi
      setup_self_signed_keystore "${file}" "${alias}" "${password}" "${password}" "${id}"
  else
      printf "[%5s]   Using existing keystore from %s\n" "${id}" "${file}"
      if [ "${password}" == "" ]; then
        echo "Password is required for existing keystore"
        exit 1
      else
        echo "Password for existing keystore=${password}"
      fi
  fi

  printf "[%5s] Updating unity config in %s\n" "${id}" "${pki_properties_file}"
  update_conf_key_value "unity.pki.credentials.${id}.format" "${type}" "${pki_properties_file}"
  update_conf_key_value "unity.pki.credentials.${id}.path" "${file}" "${pki_properties_file}"
  update_conf_key_value "unity.pki.credentials.${id}.keyAlias" "${alias}" "${pki_properties_file}"
  update_conf_key_value "unity.pki.credentials.${id}.password" "${password}" "${pki_properties_file}"
  printf "[%5s] Done\n" "${id}"
}