<#if title??>
	<title>${title}</title>
</#if>

<meta name="viewport" content="width=device-width,initial-scale=1.0">

<style type="text/css">html, body {height:100%;margin:0;}</style>
<!--
<link rel="icon" type="image/png" sizes="16x16" href="./VAADIN/themes/common/img/favicon/favicon-16.png">
<link rel="icon" type="image/png" sizes="24x24" href="./VAADIN/themes/common/img/favicon/favicon-24.png">
-->
<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="./VAADIN/themes/unityThemeValo/favicon.ico">
<link rel="icon" type="image/vnd.microsoft.icon" href="./VAADIN/themes/unityThemeValo/favicon.ico">

<!--<link rel="stylesheet" type="text/css" href="./VAADIN/themes/unityThemeValo/font-awesome.min.css">-->
<link rel="stylesheet" type="text/css" href="./VAADIN/themes/unityThemeValo/bootstrap.min.css">
