<!DOCTYPE html>
<html>
  <head>
    <#include "system/header-mandatory.ftl">
    <#include "system/header-std.ftl">
  </head>

  <body>

    <!-- Custom CLARIN header -->
    <header id="header" role="banner" class="header">
        <div class="clarin-top visible-xs">
            <a class="logo center-block" href="http://www.clarin.eu"></a>
        </div>
        <span class="qualifier snapshot">TESTING</span>
        <span class="qualifier beta">BETA</span>
        <div class="navbar-static-top  navbar-default navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#id1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/" id="idd2">
                        <span>
                            <i class="fa fa-shield" aria-hidden="true"></i>
                            CLARIN Identity Provider
                        </span>
                    </a>
                </div>
                <div class="collapse navbar-collapse" role="navigation" id="id1">
                    <ul class="nav navbar-nav" id="idd3">
                        <li>
                            <a href="https://www.clarin.eu/content/clarin-identity-provider-help">Help</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="idd4">
                        <li>
                            <a href="http://www.clarin.eu/" class="clarin-logo hidden-xs">
                                <span>CLARIN</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- Custom CLARIN header -->

    <#assign appId="main-element">
    <#include "system/body-main-ui.ftl">
    <#include "system/body-mandatory-end.ftl">

    <!-- Custom CLARIN footer -->
    <div id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-3 col-xs-12">
                    <div class="text-center">
                        <span class="footer-fineprint">
                            Service provided by <a href="https://www.clarin.eu">CLARIN</a>
                            <br />
                            Powered by <a href="http://www.unity-idm.eu/">Unity - Identity relationship management</a>
                        </span>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-pull-6 col-xs-12">
                    <div class="hidden-xs">
                        <a href="https://www.clarin.eu/content/clarin-identity-provider">About</a>
                    </div>
                    <div class="version-info text-center-xs">
                        {{VERSION}}
                    </div>
                </div>
                <div class="col-sm-3 hidden-xs text-right">
                    <a href="mailto:accounts@clarin.eu">Contact</a>
                </div>
                <div class="visible-xs-block text-center">
                    <a href="./about">About</a>
                    &nbsp;<a href="mailto:accounts@clarin.eu">Contact</a>
                </div>
            </div>
        </div>
    </div>
    <!-- Custom CLARIN footer -->

    <!-- Piwik -->
    <script type="text/javascript">
            var _paq = _paq || [];
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                    var u="https://stats.clarin.eu/";
                    _paq.push(['setTrackerUrl', u+'piwik.php']);
                    _paq.push(['setSiteId', -1]);
                    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
    </script>
    <noscript><p><img src="https://stats.clarin.eu/piwik.php?idsite=8" style="border:0;" alt="" /></p></noscript>
    <!-- End Piwik Code -->

  </body>
</html