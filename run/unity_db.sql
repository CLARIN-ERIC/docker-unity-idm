--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 9.6.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: attribute_types; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.attribute_types (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    value_syntax_id character varying(64)
);


ALTER TABLE public.attribute_types OWNER TO unity;

--
-- Name: attribute_types_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.attribute_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attribute_types_id_seq OWNER TO unity;

--
-- Name: attribute_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.attribute_types_id_seq OWNED BY public.attribute_types.id;


--
-- Name: attributes; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.attributes (
    id integer NOT NULL,
    type_id integer NOT NULL,
    entity_id integer NOT NULL,
    group_id integer NOT NULL,
    contents bytea
);


ALTER TABLE public.attributes OWNER TO unity;

--
-- Name: attributes_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.attributes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attributes_id_seq OWNER TO unity;

--
-- Name: attributes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.attributes_id_seq OWNED BY public.attributes.id;


--
-- Name: attributes_lookup; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.attributes_lookup (
    id integer NOT NULL,
    keyword character varying(100),
    attribute_id integer NOT NULL
);


ALTER TABLE public.attributes_lookup OWNER TO unity;

--
-- Name: attributes_lookup_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.attributes_lookup_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.attributes_lookup_id_seq OWNER TO unity;

--
-- Name: attributes_lookup_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.attributes_lookup_id_seq OWNED BY public.attributes_lookup.id;


--
-- Name: audit_entities; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.audit_entities (
    id integer NOT NULL,
    entity_id integer NOT NULL,
    name character varying(200),
    email character varying(200)
);


ALTER TABLE public.audit_entities OWNER TO unity;

--
-- Name: audit_entities_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.audit_entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_entities_id_seq OWNER TO unity;

--
-- Name: audit_entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.audit_entities_id_seq OWNED BY public.audit_entities.id;


--
-- Name: audit_events; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.audit_events (
    id integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    type character varying(100) NOT NULL,
    name character varying(200) NOT NULL,
    subject_id integer,
    initiator_id integer NOT NULL,
    action character varying(100) NOT NULL,
    contents bytea
);


ALTER TABLE public.audit_events OWNER TO unity;

--
-- Name: audit_events_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.audit_events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_events_id_seq OWNER TO unity;

--
-- Name: audit_events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.audit_events_id_seq OWNED BY public.audit_events.id;


--
-- Name: audit_events_tags; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.audit_events_tags (
    event_id integer NOT NULL,
    tag_id integer
);


ALTER TABLE public.audit_events_tags OWNER TO unity;

--
-- Name: audit_tags; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.audit_tags (
    id integer NOT NULL,
    tag character varying(200) NOT NULL
);


ALTER TABLE public.audit_tags OWNER TO unity;

--
-- Name: audit_tags_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.audit_tags_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.audit_tags_id_seq OWNER TO unity;

--
-- Name: audit_tags_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.audit_tags_id_seq OWNED BY public.audit_tags.id;


--
-- Name: entities; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.entities (
    id integer NOT NULL,
    name character varying(200),
    contents bytea
);


ALTER TABLE public.entities OWNER TO unity;

--
-- Name: entities_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entities_id_seq OWNER TO unity;

--
-- Name: entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.entities_id_seq OWNED BY public.entities.id;


--
-- Name: events_queue; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.events_queue (
    id integer NOT NULL,
    listener_id character varying(256),
    next_processing timestamp without time zone,
    failures integer,
    contents bytea
);


ALTER TABLE public.events_queue OWNER TO unity;

--
-- Name: events_queue_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.events_queue_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.events_queue_id_seq OWNER TO unity;

--
-- Name: events_queue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.events_queue_id_seq OWNED BY public.events_queue.id;


--
-- Name: files; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.files (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    owner_type character varying(100),
    owner_id character varying(100),
    last_update timestamp without time zone NOT NULL
);


ALTER TABLE public.files OWNER TO unity;

--
-- Name: files_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.files_id_seq OWNER TO unity;

--
-- Name: files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.files_id_seq OWNED BY public.files.id;


--
-- Name: group_entities; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.group_entities (
    entity_id integer NOT NULL,
    group_id integer NOT NULL,
    contents bytea
);


ALTER TABLE public.group_entities OWNER TO unity;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    parent_id integer
);


ALTER TABLE public.groups OWNER TO unity;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO unity;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: identities; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.identities (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    type_id integer,
    entity_id integer NOT NULL
);


ALTER TABLE public.identities OWNER TO unity;

--
-- Name: identities_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.identities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.identities_id_seq OWNER TO unity;

--
-- Name: identities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.identities_id_seq OWNED BY public.identities.id;


--
-- Name: identity_types; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.identity_types (
    id integer NOT NULL,
    name character varying(200),
    contents bytea
);


ALTER TABLE public.identity_types OWNER TO unity;

--
-- Name: identity_types_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.identity_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.identity_types_id_seq OWNER TO unity;

--
-- Name: identity_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.identity_types_id_seq OWNED BY public.identity_types.id;


--
-- Name: idp_statistics; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.idp_statistics (
    id integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    idp_endpoint_id character varying(300) NOT NULL,
    idp_endpoint_name character varying(300),
    client_id character varying(300) NOT NULL,
    client_name character varying(300),
    status character varying(100) NOT NULL
);


ALTER TABLE public.idp_statistics OWNER TO unity;

--
-- Name: idp_statistics_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.idp_statistics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.idp_statistics_id_seq OWNER TO unity;

--
-- Name: idp_statistics_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.idp_statistics_id_seq OWNED BY public.idp_statistics.id;


--
-- Name: messages; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.messages (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    locale character varying(100)
);


ALTER TABLE public.messages OWNER TO unity;

--
-- Name: messages_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.messages_id_seq OWNER TO unity;

--
-- Name: messages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.messages_id_seq OWNED BY public.messages.id;


--
-- Name: policy_documents; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.policy_documents (
    id integer NOT NULL,
    name character varying(200),
    contents bytea
);


ALTER TABLE public.policy_documents OWNER TO unity;

--
-- Name: policy_documents_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.policy_documents_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.policy_documents_id_seq OWNER TO unity;

--
-- Name: policy_documents_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.policy_documents_id_seq OWNED BY public.policy_documents.id;


--
-- Name: tokens; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.tokens (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    type character varying(100),
    entity_id integer,
    created timestamp without time zone NOT NULL,
    expires timestamp without time zone
);


ALTER TABLE public.tokens OWNER TO unity;

--
-- Name: tokens_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.tokens_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tokens_id_seq OWNER TO unity;

--
-- Name: tokens_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.tokens_id_seq OWNED BY public.tokens.id;


--
-- Name: uvos_flag; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.uvos_flag (
    val character varying(128)
);


ALTER TABLE public.uvos_flag OWNER TO unity;

--
-- Name: var_objects; Type: TABLE; Schema: public; Owner: unity
--

CREATE TABLE public.var_objects (
    id integer NOT NULL,
    name character varying(200),
    contents bytea,
    type character varying(100),
    last_update timestamp without time zone NOT NULL
);


ALTER TABLE public.var_objects OWNER TO unity;

--
-- Name: var_objects_id_seq; Type: SEQUENCE; Schema: public; Owner: unity
--

CREATE SEQUENCE public.var_objects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.var_objects_id_seq OWNER TO unity;

--
-- Name: var_objects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: unity
--

ALTER SEQUENCE public.var_objects_id_seq OWNED BY public.var_objects.id;


--
-- Name: attribute_types id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attribute_types ALTER COLUMN id SET DEFAULT nextval('public.attribute_types_id_seq'::regclass);


--
-- Name: attributes id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes ALTER COLUMN id SET DEFAULT nextval('public.attributes_id_seq'::regclass);


--
-- Name: attributes_lookup id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes_lookup ALTER COLUMN id SET DEFAULT nextval('public.attributes_lookup_id_seq'::regclass);


--
-- Name: audit_entities id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_entities ALTER COLUMN id SET DEFAULT nextval('public.audit_entities_id_seq'::regclass);


--
-- Name: audit_events id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events ALTER COLUMN id SET DEFAULT nextval('public.audit_events_id_seq'::regclass);


--
-- Name: audit_tags id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_tags ALTER COLUMN id SET DEFAULT nextval('public.audit_tags_id_seq'::regclass);


--
-- Name: entities id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.entities ALTER COLUMN id SET DEFAULT nextval('public.entities_id_seq'::regclass);


--
-- Name: events_queue id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.events_queue ALTER COLUMN id SET DEFAULT nextval('public.events_queue_id_seq'::regclass);


--
-- Name: files id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.files ALTER COLUMN id SET DEFAULT nextval('public.files_id_seq'::regclass);


--
-- Name: groups id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: identities id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identities ALTER COLUMN id SET DEFAULT nextval('public.identities_id_seq'::regclass);


--
-- Name: identity_types id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identity_types ALTER COLUMN id SET DEFAULT nextval('public.identity_types_id_seq'::regclass);


--
-- Name: idp_statistics id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.idp_statistics ALTER COLUMN id SET DEFAULT nextval('public.idp_statistics_id_seq'::regclass);


--
-- Name: messages id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.messages ALTER COLUMN id SET DEFAULT nextval('public.messages_id_seq'::regclass);


--
-- Name: policy_documents id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.policy_documents ALTER COLUMN id SET DEFAULT nextval('public.policy_documents_id_seq'::regclass);


--
-- Name: tokens id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.tokens ALTER COLUMN id SET DEFAULT nextval('public.tokens_id_seq'::regclass);


--
-- Name: var_objects id; Type: DEFAULT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.var_objects ALTER COLUMN id SET DEFAULT nextval('public.var_objects_id_seq'::regclass);


--
-- Name: attribute_types attribute_types_name_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attribute_types
    ADD CONSTRAINT attribute_types_name_key UNIQUE (name);


--
-- Name: attribute_types attribute_types_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attribute_types
    ADD CONSTRAINT attribute_types_pkey PRIMARY KEY (id);


--
-- Name: attributes_lookup attributes_lookup_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes_lookup
    ADD CONSTRAINT attributes_lookup_pkey PRIMARY KEY (id);


--
-- Name: attributes attributes_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attributes_pkey PRIMARY KEY (id);


--
-- Name: audit_entities audit_entities_entity_id_name_email_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_entities
    ADD CONSTRAINT audit_entities_entity_id_name_email_key UNIQUE (entity_id, name, email);


--
-- Name: audit_entities audit_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_entities
    ADD CONSTRAINT audit_entities_pkey PRIMARY KEY (id);


--
-- Name: audit_events audit_events_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events
    ADD CONSTRAINT audit_events_pkey PRIMARY KEY (id);


--
-- Name: audit_events_tags audit_events_tags_event_id_tag_id_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events_tags
    ADD CONSTRAINT audit_events_tags_event_id_tag_id_key UNIQUE (event_id, tag_id);


--
-- Name: audit_tags audit_tags_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_tags
    ADD CONSTRAINT audit_tags_pkey PRIMARY KEY (id);


--
-- Name: audit_tags audit_tags_tag_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_tags
    ADD CONSTRAINT audit_tags_tag_key UNIQUE (tag);


--
-- Name: entities entities_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_pkey PRIMARY KEY (id);


--
-- Name: events_queue events_queue_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.events_queue
    ADD CONSTRAINT events_queue_pkey PRIMARY KEY (id);


--
-- Name: files files_name_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_name_key UNIQUE (name);


--
-- Name: files files_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.files
    ADD CONSTRAINT files_pkey PRIMARY KEY (id);


--
-- Name: group_entities group_entities_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.group_entities
    ADD CONSTRAINT group_entities_pkey PRIMARY KEY (entity_id, group_id);


--
-- Name: groups groups_name_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_name_key UNIQUE (name);


--
-- Name: groups groups_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: identities identities_name_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_name_key UNIQUE (name);


--
-- Name: identities identities_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_pkey PRIMARY KEY (id);


--
-- Name: identity_types identity_types_name_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identity_types
    ADD CONSTRAINT identity_types_name_key UNIQUE (name);


--
-- Name: identity_types identity_types_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identity_types
    ADD CONSTRAINT identity_types_pkey PRIMARY KEY (id);


--
-- Name: idp_statistics idp_statistics_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.idp_statistics
    ADD CONSTRAINT idp_statistics_pkey PRIMARY KEY (id);


--
-- Name: messages messages_name_locale_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_name_locale_key UNIQUE (name, locale);


--
-- Name: messages messages_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);


--
-- Name: policy_documents policy_documents_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.policy_documents
    ADD CONSTRAINT policy_documents_pkey PRIMARY KEY (id);


--
-- Name: tokens tokens_name_type_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_name_type_key UNIQUE (name, type);


--
-- Name: tokens tokens_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (id);


--
-- Name: var_objects var_objects_name_type_key; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.var_objects
    ADD CONSTRAINT var_objects_name_type_key UNIQUE (name, type);


--
-- Name: var_objects var_objects_pkey; Type: CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.var_objects
    ADD CONSTRAINT var_objects_pkey PRIMARY KEY (id);


--
-- Name: attributes_entity_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX attributes_entity_id_idx ON public.attributes USING btree (entity_id);


--
-- Name: attributes_group_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX attributes_group_id_idx ON public.attributes USING btree (group_id);


--
-- Name: attributes_lookup_keyword_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX attributes_lookup_keyword_idx ON public.attributes_lookup USING btree (keyword);


--
-- Name: attributes_type_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX attributes_type_id_idx ON public.attributes USING btree (type_id);


--
-- Name: audit_events_timestamp_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX audit_events_timestamp_idx ON public.audit_events USING btree ("timestamp");


--
-- Name: events_dates_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX events_dates_idx ON public.events_queue USING btree (next_processing);


--
-- Name: group_entities_entity_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX group_entities_entity_id_idx ON public.group_entities USING btree (entity_id);


--
-- Name: group_entities_group_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX group_entities_group_id_idx ON public.group_entities USING btree (group_id);


--
-- Name: groups_parent_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX groups_parent_id_idx ON public.groups USING btree (parent_id);


--
-- Name: identities_entity_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX identities_entity_id_idx ON public.identities USING btree (entity_id);


--
-- Name: identities_type_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX identities_type_id_idx ON public.identities USING btree (type_id);


--
-- Name: tokens_entity_id_idx; Type: INDEX; Schema: public; Owner: unity
--

CREATE INDEX tokens_entity_id_idx ON public.tokens USING btree (entity_id);


--
-- Name: attributes attributes_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attributes_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: attributes attributes_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attributes_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: attributes_lookup attributes_lookup_attribute_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes_lookup
    ADD CONSTRAINT attributes_lookup_attribute_id_fkey FOREIGN KEY (attribute_id) REFERENCES public.attributes(id) ON DELETE CASCADE;


--
-- Name: attributes attributes_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.attributes
    ADD CONSTRAINT attributes_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.attribute_types(id) ON DELETE CASCADE;


--
-- Name: audit_events audit_events_initiator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events
    ADD CONSTRAINT audit_events_initiator_id_fkey FOREIGN KEY (initiator_id) REFERENCES public.audit_entities(id) ON DELETE CASCADE;


--
-- Name: audit_events audit_events_subject_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events
    ADD CONSTRAINT audit_events_subject_id_fkey FOREIGN KEY (subject_id) REFERENCES public.audit_entities(id) ON DELETE CASCADE;


--
-- Name: audit_events_tags audit_events_tags_event_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events_tags
    ADD CONSTRAINT audit_events_tags_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.audit_events(id) ON DELETE CASCADE;


--
-- Name: audit_events_tags audit_events_tags_tag_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.audit_events_tags
    ADD CONSTRAINT audit_events_tags_tag_id_fkey FOREIGN KEY (tag_id) REFERENCES public.audit_tags(id) ON DELETE CASCADE;


--
-- Name: group_entities group_entities_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.group_entities
    ADD CONSTRAINT group_entities_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: group_entities group_entities_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.group_entities
    ADD CONSTRAINT group_entities_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: groups groups_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.groups(id) ON DELETE CASCADE;


--
-- Name: identities identities_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: identities identities_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.identities
    ADD CONSTRAINT identities_type_id_fkey FOREIGN KEY (type_id) REFERENCES public.identity_types(id) ON DELETE CASCADE;


--
-- Name: tokens tokens_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: unity
--

ALTER TABLE ONLY public.tokens
    ADD CONSTRAINT tokens_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--

