#!/bin/bash

set -e

#remove all occurences with:
# "entityId": xxx,
# "entity": xxx,

cp unity-dbdump-20190617-06242706.json.orig filtered.json

duplicates=$(< filtered.json jq '.contents.identities[] | .contents.value' | tr '[:upper:]' '[:lower:]' | sort | uniq -d)
while read -r line; do
	identities=$(< filtered.json jq ".contents.identities[] | select(.contents.value | ascii_downcase == $line) | .contents.value")
	while read -r identity; do
		entity_id=$(< filtered.json jq ".contents.identities[] | select(.contents.value == ${identity}) | .entityId")
		if [[ $identity =~ [[:upper:]] ]]; then
			echo "Removing identity=${identity} with entityId=${entity_id}:"
			echo "  Removing entities" && < filtered.json jq "del(.contents.entities[] | select(.id == ${entity_id}))" > filtered.json.tmp && mv filtered.json.tmp filtered.json
			echo "  Removing identities" && < filtered.json jq "del(.contents.identities[] | select(.entityId == ${entity_id}))" > filtered.json.tmp && mv filtered.json.tmp filtered.json
			echo "  Removing from groups" && < filtered.json jq "del(.contents.groupMembers[] | .members[] | select(.entity == ${entity_id}))" > filtered.json.tmp && mv filtered.json.tmp filtered.json
			echo "  Removing from attributes" && < filtered.json jq "del(.contents.attributes[] | select(.entity == ${entity_id}))" > filtered.json.tmp && mv filtered.json.tmp filtered.json
		fi
	done <<< "${identities}"
done <<< "${duplicates}"

echo "  Removing targetedPersistent identities" && < filtered.json jq 'del(.contents.identities[] | select(.typeName == "targetedPersistent"))' > filtered.json.tmp && mv filtered.json.tmp filtered.json
