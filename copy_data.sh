#!/bin/bash

set -e

GO_PATH="/go"
build_image="registry.gitlab.com/wjm.elbers/golang-build:1.0.0-debian"

build() {
 # project_path=${1}
  binary="${2}"

  printf "  Building %s.\n" "${binary}"
  docker run --rm \
    -e "GOPATH=${GO_PATH}" \
    -e "BINARY=${binary}" \
    -v "$PWD/golang/":/go/ \
    -v "$PWD/output":/output \
    -w "/go/src/${binary}" \
    ${build_image} sh -c 'make'
  printf " Done.\n"
  cp "$PWD/output/unity-client_amd64_linux" "$PWD/image/unity-client_amd64_linux"
  cp "$PWD/output/unity-client_arm64_linux" "$PWD/image/unity-client_arm64_linux"
  chmod ugo+x "$PWD"/image/unity-client_*_linux
}

build_all_binaries() {
  #Sourced via build script, so no arrays supported
  set -e
  echo "Building binaries:"
  cd ..
  build "unity-client" "unity-client"
  cd ./image
  set +e
}

init_data() {
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    build_all_binaries
}

cleanup_data() {
    rm -f ./image/unity-client_*_linux
}
