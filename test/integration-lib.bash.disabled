#
#if [ -n "$CI_SERVER" ]; then
#
#    PROJECT_NAME="$(cd .. && basename "$(pwd)")"
#    TAG="$(git describe --always)"
#    IMAGE_QUALIFIED_NAME="$PROJECT_NAME:${TAG:-latest}"
#    export IMAGE_QUALIFIED_NAME
#fi

RED='\033[1;31m'
GREEN='\033[1;32m'
NC='\033[0m' # No Color

#Add or update key=value pair
function update_conf_key_value {
    local _key="$1"
    local _value="$2"
    local _file="$3"

    local _key_exists=$(grep "${_key}" "${_file}" | wc -l | xargs)
    if [ "${key__key_exists}" == "0" ]; then
        echo "${_key}=${_value}" >> "${_file}"
    else
        local _normalized_key=$(printf '%s' "${_key}" | sed -e 's/[]\/$*.^[]/\\&/g');
        local _normalized_value=$(printf '%s' "${_value}" | sed -e 's/[]\/$*.^[]/\\&/g');
        #https://unix.stackexchange.com/a/102539
        sed -i -e "/^${_normalized_key}/s/=.*$/=${_normalized_value}/" "${_file}"
    fi
}

function compose_start() {
    local _cmd="$1"
    local _ldap_mode="$2"

    echo "Updating .env, mode = ${_ldap_mode}"
    update_conf_key_value "LDAP_TRUSTED_MODE" "${_ldap_mode}" ".env"

    echo "Starting (IMAGE_QUALIFIED_NAME=${IMAGE_QUALIFIED_NAME})"
    #local _startup_log=$(${_cmd} up -d > startup.log 2>&1)
    local _startup_log=$(${_cmd} up -d > startup.log 2>&1)
    if [ "$?" != "0" ]; then
        echo "Failed to start all services\n"
        echo "${_startup_log}"
        exit_code=1
    fi


    error_count="$(${_cmd} ps -q | xargs docker inspect -f '{{ .State.ExitCode }}')"
    if [ "${error_count}" != "0" ]; then
        echo "Failed to start all services\n"
        echo "${_startup_log}"
        exit_code=1
    fi
}

function compose_shutdown() {
    local _cmd="$1"

    if [ "${SLEEP}" != "" ]; then
        printf "Sleeping for ${SLEEP} seconds before shutting down."
        sleep ${SLEEP}
    fi

    echo "Shutting down"
    local _shutdown_log=$(${_cmd} down -v 2>&1)
    if [ "$?" != "0" ]; then
        echo "Failed to shutdown"
        echo "${_shutdown_log}"
        exit_code=1
    fi
}

function print_compose_log() {
    ${cmd} logs
}

function prepare_tests() {
    local _cmd="$1"

    #Wait for services to start
    tick_sleep=1
    tick_limit=600 #with a 1 second tick, this equals to 120 * 1 seconds = 120 seconds or 2 minutes
    tick_count=0
    printf 'Waiting for unity to start (timeout=%s seconds)' "$(( tick_count + tick_limit ))"
    until $(${_cmd} exec -T unity curl --output /dev/null --silent --head --fail --insecure https://unity:2443/home); do
        if (( $tick_count < $tick_limit )); then
            printf '.'
            tick_count=$(( tick_count + 1 ))
            sleep ${tick_sleep}
        else
            printf "\nTimeout exceeded\n"
            ${cmd} logs --no-log-prefix apache-ldap > ldap.log
            ${cmd} logs --no-log-prefix unity > unity.log
            ${cmd} logs --no-log-prefix database > database.log

            echo "Startup:"
            cat startup.log
            echo "Unity:"
            cat unity.log

            compose_shutdown "${_cmd}"
            exit 1
        fi
    done
    printf '\n'
    echo "Unity up and running in $(( tick_count + tick_sleep )) seconds."
    printf "Preparing user accounts\n"

    sleep 5

    #Prepare user accounts
    account=$(${_cmd} exec -T unity /clarin-unity-cli clarin user random --amount 1 --prefix test_a)
    #Capture the username, entityid and password from the response
    regex="Created user: (.+)\. EntityId=(.+), password=([[:print:]]+).*"
    [[ $account =~ $regex ]]

    username=${BASH_REMATCH[1]}
    password=${BASH_REMATCH[3]}

    echo "Account name=[${username}], password=[${password}]"

    if [ "${password}" == "" ]; then
        echo "Password for generated account cannot be empty, aborting tests."
        print_compose_log
        compose_shutdown "${_cmd}"
        exit 1
    fi
}

function run_tests() {
    local _cmd="${1}"
    local test_result=0

    printf "Access protected location without credentials:      "
    http_response=$(${_cmd} exec -T apache-ldap curl --output /dev/null --insecure --silent --fail -w "%{http_code}" https://localhost 2> /dev/null)
    result="${RED}FAILED${NC}"
    if [ "401" == "${http_response}" ]; then
        result="${GREEN}PASSED${NC}"
    else
        test_result=1
        write_logs "${_cmd}"
    fi
    printf "Expected response HTTP 401, received: HTTP ${http_response}. ${result}\n"

    printf "Access protected location with invalid credentials: "
    http_response=$(${_cmd} exec -T apache-ldap curl --output /dev/null --insecure --silent --fail --user "${username}:${password}wrong" -w "%{http_code}" https://localhost 2> /dev/null)
    result="${RED}FAILED${NC}"
    if [ "401" == "${http_response}" ]; then
        result="${GREEN}PASSED${NC}"
    else
        test_result=1
        write_logs "${_cmd}"
    fi
    printf "Expected response HTTP 401, received: HTTP ${http_response}. ${result}\n"

    printf "Access protected location with valid credentials:   "
    http_response=$(${_cmd} exec -T apache-ldap curl --output /dev/null --insecure --silent --fail --user "${username}:${password}" -w "%{http_code}" https://localhost 2> /dev/null)
    result="${RED}FAILED${NC}"
    if [ "200" == "${http_response}" ]; then
        result="${GREEN}PASSED${NC}"
    else
        test_result=1
        write_logs "${_cmd}"
    fi
    printf "Expected response HTTP 200, received: HTTP ${http_response}. ${result}\n"

    return ${test_result}
}

function write_logs() {
    local _cmd="${1}"

    ${_cmd} logs --no-log-prefix apache-ldap > ldap.log
    ${_cmd} logs --no-log-prefix unity > unity.log
    ${_cmd} logs --no-log-prefix database > database.log
}
