<!DOCTYPE html>
<html>
  <head>
    <#include "system/header-mandatory.ftl">
    <#include "system/header-std.ftl">

    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab' rel='stylesheet' type='text/css' />

    <link rel="stylesheet" type="text/css" href="./VAADIN/themes/clarinTheme/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="./VAADIN/themes/clarinTheme/clarin.css">
    <title>CLARIN Identity Provider</title>
    <style>
        html, body {
            margin: 0;
            width: 100%;
            height: 100%;
            color: #07426e;
            display: table;
        }

        #header {
            display: table-row;
        }

        #footer {
            min-height: 3em;
            padding-bottom: 0.5em;
            padding-top: 0.5em;
            margin-top: 30px;
        }

        .max-height {
            height: 100%
        }

        .v-app {
        }

        .unityThemeValo .u-header {
            background: none !important;
        }

        .v-label-u-textEndpointHeading {
                display: none !important;
        }

        .v-slot-u-authn-logo {
            display: none !important;
        }

        .v-slot-u-authn-title {
            display: none !important;

        }

{{ with .alpha }}
        #header .qualifier.snapshot {
            display: block !important;
            left: 0.5em !important;
        }
{{ end }}

{{ with .beta }}
        #header .qualifier.beta {
            display: block !important;
            left: 0.5em !important;
        }
{{ end }}

        .navbar {
            margin-bottom: 0px;
        }

        .table-row {
            display: table-row;
        }

        p {
            word-break: normal;
            white-space: normal;
        }

        .u-standalone-public-form {
		    width: 75% !important;
		    display: block !important;
		    margin: auto;
	    }

        .v-tooltip {
            background-color: rgba(51, 51, 51, .8) !important;
            padding: 0px;
            border: none;
        }

	    .v-tooltip-text {
             background-color: rgba(255, 255, 255, 0) !important;
             border: none;
        }

        .v-tooltip-pre {
            background-color: rgba(255, 255, 255, 0) !important;
            color: #ffffff;
            padding: 3px;
            border: none;
        }

        .v-icon {
            color: #07426e !important;
        }
    </style>
  </head>
 
  <body>

     <!-- Custom CLARIN header -->
     <header id="header" role="banner">
            <div class="clarin-top visible-xs">
                <a class="logo center-block" href="http://www.clarin.eu"></a>
            </div>
            <span class="qualifier snapshot">TESTING</span>
            <span class="qualifier beta">BETA</span>
            <div class="navbar-static-top  navbar-default navbar" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#id1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/user/home" id="idd2">
                        <span>
                            <i class="fa fa-shield" aria-hidden="true"></i>
                            CLARIN Identity Provider
                        </span>
                    </a>
                </div>
                <div class="collapse navbar-collapse" role="navigation" id="id1">
                    <ul class="nav navbar-nav" id="idd3">
                        <li>
                            <a href="https://www.clarin.eu/content/clarin-identity-provider-help">Help</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" id="idd4">
                        <li>
                            <a href="http://www.clarin.eu/" class="clarin-logo hidden-xs">
                                <span>CLARIN</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </header>


    <!-- Main UI -->
    <#assign appId="main-element">
    <#include "system/body-main-ui.ftl">

    <!-- Custom CLARIN footer -->
    <div class="table-row">
        <div id="footer">
            <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-push-3 col-xs-12">
                            <div class="text-center">
                                <span class="footer-fineprint">
                                    Service provided by <a href="https://www.clarin.eu">CLARIN</a>
                                    <br />
                                    Powered by <a href="http://www.unity-idm.eu/">Unity - Identity relationship management</a>
                                </span>
                            </div>
                        </div>
                        <div class="col-sm-3 col-sm-pull-6 col-xs-12">
                            <div class="hidden-xs">
                                <a href="https://www.clarin.eu/content/clarin-identity-provider">About</a>
                            </div>
                            <div class="version-info text-center-xs">
{{ .version }}
                            </div>
                        </div>
                        <div class="col-sm-3 hidden-xs text-right">
                            <a href="mailto:accounts@clarin.eu">Contact</a>
                        </div>
                        <div class="visible-xs-block text-center">
                            <a href="./about">About</a>
                            &nbsp;<a href="mailto:accounts@clarin.eu">Contact</a>
                        </div>
                    </div>
                </div>
        </div>
    </div>

    <!-- Scripts -->
    <#include "system/body-mandatory-end.ftl">

    <!-- Piwik -->
    <script type="text/javascript">
            var _paq = _paq || [];
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
                    var u="https://stats.clarin.eu/";
                    _paq.push(['setTrackerUrl', u+'piwik.php']);
                    _paq.push(['setSiteId', 8]);
                    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
                    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
            })();
    </script>
    <noscript><p><img src="https://stats.clarin.eu/piwik.php?idsite=8" style="border:0;" alt="" /></p></noscript>
    <!-- End Piwik Code -->

  </body>
</html>
